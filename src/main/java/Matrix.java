public class Matrix<T> {

    private int size;
    private T[][] matrix;

    Matrix(T[][] customMatrix) {
        this.size = customMatrix.length;
        this.matrix = customMatrix;
    }

    public void printMatrix() {

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + "  ");
            }
            System.out.println();
        }
    }

    public void printMirrorMatrix() {
        int n = 0;

        for (int i = 0; i < size; i++) {
            n++;

            //���� ��� ������ �������� ����� ���������� ���������� �������
            for (int k = 0; k < size - n; k++) {
                System.out.print("   ");
            }

            for (int j = 0; j < n; j++) {
                System.out.print(matrix[i - j][size - n] + "  ");
            }
            System.out.println();
        }
    }
}
