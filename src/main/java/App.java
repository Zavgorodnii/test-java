public class App {

    public static void main(String[] args) {

        Integer[][] customMatrix =  {{1, 0, 3, 4},
                                     {5, 0, 7},
                                     {8, 9},
                                     {2}};

        Character[][] customMatrix2 =   {{'a', 'a', 'y', '4'},
                                         {'k', 'k', '+'},
                                         {'-', '#'},
                                         {'^'}};

        Matrix<Integer> matrix = new Matrix<Integer>(customMatrix);
        matrix.printMatrix();
        matrix.printMirrorMatrix();

        System.out.println();

        Matrix<Character> matrix2 = new Matrix<Character>(customMatrix2);
        matrix2.printMatrix();
        matrix2.printMirrorMatrix();
    }
}
